﻿using Microsoft.EntityFrameworkCore;
using Spg.TestAdministrator.Business.Application;
using Spg.TestAdministrator.Business.Domain.Exceptions;
using Spg.TestAdministrator.Business.Domain.Models;
using Spg.TestAdministrator.Business.Infrastructure;
using Spg.TestAdministrator.Business.Test.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Spg.TestAdministrator.Business.Test
{
    public class ExamServiceTest
    {
        private TestsAdministratorContext GetDbContext()
        {
            // Test-DB erstellen
            DbContextOptionsBuilder<TestsAdministratorContext> dbOptions = new DbContextOptionsBuilder<TestsAdministratorContext>()
                .UseSqlite($"Data Source=TestsAdministrator_Test.db");

            TestsAdministratorContext dbContext = new TestsAdministratorContext(dbOptions.Options);

            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();

            // Test-DB seeden
            dbContext.CatAccountStates.Add(new CatAccountState() { CatAccountStateId = new Guid("64ac80ef-fb9a-413c-b6e6-e86e457b5589"), Description = "Aktiv", Key = "A", ShortName = "Aktiv" });
            dbContext.SaveChanges();

            dbContext.CatTestStates.Add(new CatTestState() { CatTestStateId = new Guid("57f44cbe-8941-4a4d-be5a-27d92309a452"), Description = "Korrigiert", Key = "C", ShortName = "Korrigiert" });
            dbContext.SaveChanges();

            dbContext.Teachers.Add(new Teacher() { T_ID = "AF", T_Account = "akyildiz", T_CatAccountStateId = new Guid("64ac80ef-fb9a-413c-b6e6-e86e457b5589"), T_Email = "akyildiz@spengergasse.at", T_Firstname = "Fatma", T_Lastname = "Akyildiz" });
            dbContext.Teachers.Add(new Teacher() { T_ID = "AGU", T_Account = "augustin", T_CatAccountStateId = new Guid("64ac80ef-fb9a-413c-b6e6-e86e457b5589"), T_Email = "augustin@spengergasse.at", T_Firstname = "Susanne", T_Lastname = "Augustin" });
            dbContext.Teachers.Add(new Teacher() { T_ID = "AH", T_Account = "auinger", T_CatAccountStateId = new Guid("64ac80ef-fb9a-413c-b6e6-e86e457b5589"), T_Email = "auinger@spengergasse.at", T_Firstname = "Harald", T_Lastname = "Auinger" });
            dbContext.Teachers.Add(new Teacher() { T_ID = "AMA", T_Account = "amcha", T_CatAccountStateId = new Guid("64ac80ef-fb9a-413c-b6e6-e86e457b5589"), T_Email = "amcha@spengergasse.at", T_Firstname = "Alfred", T_Lastname = "Amcha" });
            dbContext.SaveChanges();

            dbContext.Schoolclasses.Add(new Schoolclass() { C_ID = "1AHIF", C_Department = "HIF", C_ClassTeacher = "AH" });
            dbContext.Schoolclasses.Add(new Schoolclass() { C_ID = "2BHIF", C_Department = "HIF", C_ClassTeacher = "AGU" });
            dbContext.Schoolclasses.Add(new Schoolclass() { C_ID = "4AHIF", C_Department = "HIF", C_ClassTeacher = "AMA" });
            dbContext.Schoolclasses.Add(new Schoolclass() { C_ID = "1CHIF", C_Department = "HIF", C_ClassTeacher = "AF" });
            dbContext.Schoolclasses.Add(new Schoolclass() { C_ID = "1EHIF", C_Department = "HIF", C_ClassTeacher = "AF" });
            dbContext.SaveChanges();

            dbContext.Periods.Add(new Period() { P_From = new DateTime(2020, 09, 27, 08, 00, 00), P_To = new DateTime(2020, 09, 27, 08, 50, 00) });
            dbContext.Periods.Add(new Period() { P_From = new DateTime(2020, 09, 27, 08, 50, 00), P_To = new DateTime(2020, 09, 27, 09, 40, 00) });
            dbContext.Periods.Add(new Period() { P_From = new DateTime(2020, 09, 27, 09, 55, 00), P_To = new DateTime(2020, 09, 27, 10, 45, 00) });
            dbContext.Periods.Add(new Period() { P_From = new DateTime(2020, 09, 27, 10, 45, 00), P_To = new DateTime(2020, 09, 27, 11, 35, 00) });
            dbContext.Periods.Add(new Period() { P_From = new DateTime(2020, 09, 27, 11, 45, 00), P_To = new DateTime(2020, 09, 27, 12, 35, 00) });
            dbContext.Periods.Add(new Period() { P_From = new DateTime(2020, 09, 27, 12, 35, 00), P_To = new DateTime(2020, 09, 27, 13, 25, 00) });
            dbContext.Periods.Add(new Period() { P_From = new DateTime(2020, 09, 27, 13, 25, 00), P_To = new DateTime(2020, 09, 27, 14, 15, 00) });
            dbContext.Periods.Add(new Period() { P_From = new DateTime(2020, 09, 27, 14, 25, 00), P_To = new DateTime(2020, 09, 27, 15, 15, 00) });
            dbContext.SaveChanges();

            dbContext.Lessons.Add(new Lesson() { L_Class = "2BHIF", L_Day = 3, L_Hour = 3, L_Room = "DE.03", L_Subject = "NW2", L_Teacher = "AGU", L_Untis_ID = 1 });
            dbContext.Lessons.Add(new Lesson() { L_Class = "2BHIF", L_Day = 4, L_Hour = 2, L_Room = "C3.09", L_Subject = "NW2", L_Teacher = "AGU", L_Untis_ID = 1 });
            dbContext.Lessons.Add(new Lesson() { L_Class = "2BHIF", L_Day = 5, L_Hour = 8, L_Room = "D4.03", L_Subject = "NW2", L_Teacher = "AGU", L_Untis_ID = 1 });
            dbContext.Lessons.Add(new Lesson() { L_Class = "4AHIF", L_Day = 3, L_Hour = 2, L_Room = "C3.07", L_Subject = "AM", L_Teacher = "AMA", L_Untis_ID = 1 });
            dbContext.Lessons.Add(new Lesson() { L_Class = "4AHIF", L_Day = 3, L_Hour = 1, L_Room = "C3.07", L_Subject = "AM", L_Teacher = "AMA", L_Untis_ID = 1 });
            dbContext.Lessons.Add(new Lesson() { L_Class = "4AHIF", L_Day = 2, L_Hour = 2, L_Room = "C3.09", L_Subject = "GGPB", L_Teacher = "AMA", L_Untis_ID = 1 });
            dbContext.Lessons.Add(new Lesson() { L_Class = "1CHIF", L_Day = 1, L_Hour = 6, L_Room = "C5.10", L_Subject = "BWM2", L_Teacher = "AF", L_Untis_ID = 1 });
            dbContext.Lessons.Add(new Lesson() { L_Class = "1CHIF", L_Day = 1, L_Hour = 7, L_Room = "C5.10", L_Subject = "BWM2", L_Teacher = "AF", L_Untis_ID = 1 });
            dbContext.Lessons.Add(new Lesson() { L_Class = "1EHIF", L_Day = 3, L_Hour = 5, L_Room = "C2.14", L_Subject = "BWM1", L_Teacher = "AF", L_Untis_ID = 1 });
            dbContext.Lessons.Add(new Lesson() { L_Class = "1EHIF", L_Day = 5, L_Hour = 1, L_Room = "C4.14", L_Subject = "BWM1", L_Teacher = "AF", L_Untis_ID = 1 });
            dbContext.SaveChanges();

            dbContext.Tests.Add(new Domain.Models.Test() { Guid = new Guid("8bf2a7b9-f4d0-4b0a-aa68-698232f56398"), TE_CatTestStateId = new Guid("57f44cbe-8941-4a4d-be5a-27d92309a452"), TE_Class = "1AHIF", TE_Date = new DateTime(2021, 3, 27), TE_Subject = "NW2", TE_Teacher = "AGU", TE_Lesson = 3 });
            dbContext.Tests.Add(new Domain.Models.Test() { Guid = new Guid("2761cc63-3ecb-41e4-b8da-53eba840355d"), TE_CatTestStateId = new Guid("57f44cbe-8941-4a4d-be5a-27d92309a452"), TE_Class = "1AHIF", TE_Date = new DateTime(2021, 10, 14), TE_Subject = "Dy", TE_Teacher = "AF", TE_Lesson = 1 });
            dbContext.Tests.Add(new Domain.Models.Test() { Guid = new Guid("6bfb4cbc-c306-4a84-b012-fd4784c177a3"), TE_CatTestStateId = new Guid("57f44cbe-8941-4a4d-be5a-27d92309a452"), TE_Class = "1AHIF", TE_Date = new DateTime(2019, 9, 9), TE_Subject = "BWM1", TE_Teacher = "AGU", TE_Lesson = 2 });
            dbContext.Tests.Add(new Domain.Models.Test() { Guid = new Guid("4641d511-d68f-464e-9fb1-f0f5c8fbe7da"), TE_CatTestStateId = new Guid("57f44cbe-8941-4a4d-be5a-27d92309a452"), TE_Class = "1AHIF", TE_Date = new DateTime(2020, 5, 7), TE_Subject = "BWM2", TE_Teacher = "AF", TE_Lesson = 3 });
            dbContext.Tests.Add(new Domain.Models.Test() { Guid = new Guid("8c34b771-cf39-49a0-a0b3-433858ab1e30"), TE_CatTestStateId = new Guid("57f44cbe-8941-4a4d-be5a-27d92309a452"), TE_Class = "1AHIF", TE_Date = new DateTime(2019, 12, 30), TE_Subject = "AMx", TE_Teacher = "AF", TE_Lesson = 3 });
            dbContext.Tests.Add(new Domain.Models.Test() { Guid = new Guid("9909304f-4909-42ff-a6cf-d779cee32907"), TE_CatTestStateId = new Guid("57f44cbe-8941-4a4d-be5a-27d92309a452"), TE_Class = "1AHIF", TE_Date = new DateTime(2020, 2, 29), TE_Subject = "TINF_1", TE_Teacher = "AMA", TE_Lesson = 7 });
            dbContext.Tests.Add(new Domain.Models.Test() { Guid = new Guid("157efbf8-e91b-45d4-991b-a5f3bd1a5f45"), TE_CatTestStateId = new Guid("57f44cbe-8941-4a4d-be5a-27d92309a452"), TE_Class = "1AHIF", TE_Date = new DateTime(2020, 5, 22), TE_Subject = "TICP4A", TE_Teacher = "AF", TE_Lesson = 6 });
            dbContext.Tests.Add(new Domain.Models.Test() { Guid = new Guid("aedc2e6d-a9a9-4214-974e-224150199267"), TE_CatTestStateId = new Guid("57f44cbe-8941-4a4d-be5a-27d92309a452"), TE_Class = "1AHIF", TE_Date = new DateTime(2020, 1, 5), TE_Subject = "TICP4A", TE_Teacher = "AGU", TE_Lesson = 2 });
            dbContext.Tests.Add(new Domain.Models.Test() { Guid = new Guid("c76aacd4-3062-43ec-b3e4-ba3cadd45bbb"), TE_CatTestStateId = new Guid("57f44cbe-8941-4a4d-be5a-27d92309a452"), TE_Class = "1AHIF", TE_Date = new DateTime(2020, 4, 2), TE_Subject = "Dx", TE_Teacher = "AF", TE_Lesson = 5 });
            dbContext.Tests.Add(new Domain.Models.Test() { Guid = new Guid("e7fc29f4-b12c-4225-896d-26bda231d359"), TE_CatTestStateId = new Guid("57f44cbe-8941-4a4d-be5a-27d92309a452"), TE_Class = "1AHIF", TE_Date = new DateTime(2020, 6, 21), TE_Subject = "POS1z", TE_Teacher = "AF", TE_Lesson = 4 });
            dbContext.SaveChanges();

            return dbContext;
        }

        [Fact()]
        public async void Create_Success_Test()
        {
            // Arrange
            ExamService examService = new ExamService(GetDbContext(), new DateTimeProviderMock());
            Domain.Models.Test newEntity = new Domain.Models.Test() { Guid = new Guid("272bb2eb-3102-4459-98e8-7d3ecbb14d0c"), TE_CatTestStateId = new Guid("57f44cbe-8941-4a4d-be5a-27d92309a452"), TE_Class = "1AHIF", TE_Date = new DateTime(2021, 4, 28), TE_Subject = "POS1z", TE_Teacher = "AF", TE_Lesson = 4 };

            // Act
            await examService.CreateAsync(newEntity);

            // Assert
            Assert.Equal(11, examService.Table().Count());
        }

        [Fact()]
        public async void Create_NotInFuture_ThrowsValidationExeption_Test()
        {
            // Arrange
            ExamService examService = new ExamService(GetDbContext(), new DateTimeProviderMock());
            Domain.Models.Test newEntity = new Domain.Models.Test() { Guid = new Guid("272bb2eb-3102-4459-98e8-7d3ecbb14d0c"), TE_CatTestStateId = new Guid("57f44cbe-8941-4a4d-be5a-27d92309a452"), TE_Class = "1AHIF", TE_Date = new DateTime(2021, 4, 26), TE_Subject = "POS1z", TE_Teacher = "AF", TE_Lesson = 4 };

            // Assert (Act)
            await Assert.ThrowsAsync<ServiceValidationException>(() => examService.CreateAsync(newEntity));
        }

        [Fact()]
        public async void Create_NoSaturdaySundy_ThrowsValidationExeption_Test()
        {
            // Arrange
            ExamService examService = new ExamService(GetDbContext(), new DateTimeProviderMock());
            Domain.Models.Test newEntity = new Domain.Models.Test() { Guid = new Guid("272bb2eb-3102-4459-98e8-7d3ecbb14d0c"), TE_CatTestStateId = new Guid("57f44cbe-8941-4a4d-be5a-27d92309a452"), TE_Class = "1AHIF", TE_Date = new DateTime(2021, 5, 1), TE_Subject = "POS1z", TE_Teacher = "AF", TE_Lesson = 4 };

            // Assert (Act)
            await Assert.ThrowsAsync<ServiceValidationException>(() => examService.CreateAsync(newEntity));
        }

        // ...
        // ...
        // ...






        [Fact()]
        public void AddTest()
        {

            // HardCoded ConnectionString
            // DB Options setzen
            // Context mit den Options instanzieren

            // A A A:

            // Arrange
            ExamService examService = new ExamService(GetDbContext(), new DateTimeProviderMock());
            int expected = 8;
            int actual = 0;

            // Act
            actual = examService.Add(3, 5);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
