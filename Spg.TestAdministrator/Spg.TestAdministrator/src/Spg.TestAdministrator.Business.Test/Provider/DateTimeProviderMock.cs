﻿using Spg.TestAdministrator.Business.Domain.Interfaces.Provider;
using System;
using System.Collections.Generic;
using System.Text;

namespace Spg.TestAdministrator.Business.Test.Provider
{
    public class DateTimeProviderMock : IDateTimeProvider
    {
        public DateTime UtcNow
            => new DateTime(2021, 04, 13);
    }
}
