﻿using Spg.TestAdministrator.Business.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.Business.Application
{
    public class AuthServiceTemp : IAuthService
    {
        private readonly string _teacherId;

        public AuthServiceTemp(string teacherId)
        {
            _teacherId = teacherId;
        }

        public string Username() => throw new NotImplementedException();

        public bool IsAuthenticated => false;

        public Task LoginAsync(string username, string password)
        {
            //bool result = CheckUser(username, password);

            throw new NotImplementedException();
        }

        public Task LogOutAsync()
        {
            throw new NotImplementedException();
        }

        public string TeacherId()
        {
            return _teacherId;
        }

        public bool HasRole(string role)
        {
            throw new NotImplementedException();
        }
    }
}
