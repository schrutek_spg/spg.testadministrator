﻿using Microsoft.EntityFrameworkCore;
using Spg.TestAdministrator.Business.Domain.Exceptions;
using Spg.TestAdministrator.Business.Domain.Interfaces;
using Spg.TestAdministrator.Business.Domain.Interfaces.Provider;
using Spg.TestAdministrator.Business.Domain.Models;
using Spg.TestAdministrator.Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.Business.Application
{
    public class ExamService : ServiceBase<Test>, IReadOnlyExamService<Test>, IExamService<Test>
    {
        /// <summary>
        /// Hält die Instanz des Datenbank-Kontetes
        /// </summary>
        private readonly TestsAdministratorContext _dbContext;
        private readonly IDateTimeProvider _dateTimeProvider;

        /// <summary>
        /// Constructor-Injection
        /// </summary>
        /// <param name="dbContext"></param>
        public ExamService(TestsAdministratorContext dbContext
            , IDateTimeProvider dateTimeProvider)
            : base(dbContext)
        {
            _dbContext = dbContext;
            _dateTimeProvider = dateTimeProvider;
        }

        public Test GetById(Guid id)
        {
            Test result = Table(null, null)
                .SingleOrDefault(t => t.Guid == id);

            return result;
        }

        public async Task<Test> CreateAsync(Test newEntity)
        {
            // Überprüfungen ob der Datensatz angelegt werden darf:
            // * Test muss 2 Wochen in der Zukunft liegen
            // * Keine Tests an einem Samstag oder Sonntag
            // * Maximal 2 Tests pro Tag
            // * Keinen Test in einer Stunde, in der schon ein Test existiert
            // * Ein Lehrer darf nur für sein Fach eine Test anlegen

            if (newEntity.TE_Date < _dateTimeProvider.UtcNow.AddDays(14))
            {
                throw new ServiceValidationException("Test muss mindestens 2 Wochen vorher angekündigt werden!");
            }
            if (newEntity.TE_Date.DayOfWeek == DayOfWeek.Saturday || newEntity.TE_Date.DayOfWeek == DayOfWeek.Sunday)
            {
                throw new ServiceValidationException("Keine Tests an einem Samstag oder Sonntag!");
            }

            // ...
            // ...

            CatTestState existingTestState = await _dbContext.CatTestStates.FindAsync(new Guid("37b02a9a-9027-4a96-aba6-6ece61319cc2"));

            Test newTest = new Test();
            try
            {
                newTest.Guid = Guid.NewGuid();
                newTest.TE_Class = newEntity.TE_Class;
                newTest.TE_Date = newEntity.TE_Date;
                newTest.TE_Subject = newEntity.TE_Subject;
                newTest.TE_CatTestState = existingTestState;

                _dbContext.Add(newEntity);
                await _dbContext.SaveChangesAsync();

                return newTest;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new UpdateException("Datensatz konnte nicht gefunden werden!", ex);
            }
            catch (DbUpdateException ex)
            {
                throw new UpdateException("Änderungen konnten nicht gespeichert werden!", ex);
            }
        }

        public async Task<Test> UpdateAsync(Guid id, Test newEntity)
        {
            Test existingEntity = await _dbContext.Tests.SingleOrDefaultAsync(t => t.Guid == id)
                ?? throw new UpdateException("Datensatz konnte nicht gefunden werden!");

            try
            {
                existingEntity.Guid = Guid.NewGuid();
                existingEntity.TE_Class = newEntity.TE_Class;
                existingEntity.TE_Date = newEntity.TE_Date;
                existingEntity.TE_Subject = newEntity.TE_Subject;

                _dbContext.Update(existingEntity);
                await _dbContext.SaveChangesAsync();

                return existingEntity;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new UpdateException("Datensatz konnte nicht gefunden werden!", ex);
            }
            catch (DbUpdateException ex)
            {
                throw new UpdateException("Änderungen konnten nicht gespeichert werden!", ex);
            }
        }

        public async Task DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Echte Methode
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int Add(int x, int y)
        {
            return x + y;
        }

        /// <summary>
        /// Diese Methode testet die echte Add-Methode
        /// </summary>
        private void TestAdd()
        {
            int expected = 8;
            int actual = Add(3, 5);

            if (expected == actual)
            {
                Console.WriteLine("Methode 'int Add(int x, int y)' funktioniert!");
            }
            else
            {
                Console.WriteLine("Methode 'int Add(int x, int y)' funktioniert nich!");
            }
        }
    }
}
