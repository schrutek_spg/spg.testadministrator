﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Spg.TestAdministrator.Business.Application.Helpers
{
    public static class HashHelper
    {
        /// <summary>
        /// Generiert eine 1024 Bit lange Zufallszahl und gibt sie Base64 codiert zurück.
        /// </summary>
        public static string GenerateSalt()
        {
            byte[] secret = new byte[1024 / 8];
            using var rnd = System.Security.Cryptography.RandomNumberGenerator.Create();
            rnd.GetBytes(secret);
            return Convert.ToBase64String(secret);
        }

        /// <summary>
        /// Generiert auf Basis des gespeicherten Secrets den Passworthash.
        /// </summary>
        public static string GenerateHash(string password, string secrret)
        {
            var secret = Convert.FromBase64String(secrret);
            var dataBytes = System.Text.Encoding.UTF8.GetBytes(password);

            System.Security.Cryptography.HMACSHA256 myHash = new System.Security.Cryptography.HMACSHA256(secret);
            byte[] hashedData = myHash.ComputeHash(dataBytes);
            return Convert.ToBase64String(hashedData);
        }
    }
}
