﻿using Spg.TestAdministrator.Business.Domain.Interfaces;
using Spg.TestAdministrator.Business.Domain.Models;
using Spg.TestAdministrator.Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spg.TestAdministrator.Business.Application
{
    public class LessonService : ServiceBase<Lesson>, IReadOnlyLessonService<Lesson>
    {
        /// <summary>
        /// Hält die Instanz des Datenbank-Kontetes
        /// </summary>
        private readonly TestsAdministratorContext _dbContext;

        private readonly IAuthService _authService;

        /// <summary>
        /// Constructor-Injection
        /// </summary>
        /// <param name="dbContext"></param>
        public LessonService(TestsAdministratorContext dbContext, IAuthService authService)
            : base(dbContext)
        {
            _dbContext = dbContext;
            _authService = authService;
        }

        public IQueryable<Lesson> ListByTeacher()
        {
            if (!string.IsNullOrEmpty(_authService.TeacherId()))
            {
                return Table()
                    .Where(l => l.L_Teacher == _authService.TeacherId());
            }
            return Table();
        }
    }
}
