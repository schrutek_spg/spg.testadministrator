﻿using Spg.TestAdministrator.Business.Domain.Interfaces.Provider;
using System;
using System.Collections.Generic;
using System.Text;

namespace Spg.TestAdministrator.Business.Application.Provider
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime UtcNow
            => DateTime.UtcNow;
    }
}
