﻿using Microsoft.EntityFrameworkCore;
using Spg.TestAdministrator.Business.Domain.Exceptions;
using Spg.TestAdministrator.Business.Domain.Interfaces;
using Spg.TestAdministrator.Business.Domain.Models;
using Spg.TestAdministrator.Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.Business.Application
{
    public class SchoolclassService : ServiceBase<Schoolclass>, IReadOnlySchoolClassService<Schoolclass>
    {
        private readonly TestsAdministratorContext _context;

        public SchoolclassService(TestsAdministratorContext context)
            : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Schoolclass>> GetAllAsync()
        {
            return await _context.Schoolclasses.ToListAsync();
        }

        public async Task<Schoolclass> DetailsAsync(string id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Niemals das gesamte Model updaten!
        /// Niemals generische Excpetions verwenden
        /// </summary>
        /// <remarks>
        /// Vorgehensweise:
        /// * Vorhandenen Datensatz laden
        /// * Auf existenz prüfen
        /// * Nur Properties setzen, die geändert werden dürfen
        /// * Updaten
        /// * !SaveChanges() nicht vergessen
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="newSchoolclass"></param>
        /// <returns>Gibt die aktualisierte Schulklase zurück</returns>
        public async Task<Schoolclass> UpdateAsync(string id, Schoolclass newSchoolclass)
        {
            if (id != newSchoolclass.C_ID)
            {
                throw new UpdateException("Datensatz konnte nicht gefunden werden!");
            }

            Schoolclass existingEntity = await _context.Schoolclasses.FindAsync(id);
            if (existingEntity != null)
            {
                try
                {
                    existingEntity.C_Department = newSchoolclass.C_Department;
                    //existingEntity.C_ClassTeacher = newSchoolclass.C_ClassTeacher;

                    _context.Update(existingEntity);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    throw new UpdateException("Datensatz konnte nicht gefunden werden!", ex);
                }
                catch (DbUpdateException ex)
                {
                    throw new UpdateException("Änderungen konnten nicht gespeichert werden!", ex);
                }
            }
            throw new UpdateException("Datensatz konnte nicht gefunden werden!");
        }

        public async Task DeleteAsync(string id)
        {
            Schoolclass existingEntity = await _context.Schoolclasses.FindAsync(id) 
                ?? throw new UpdateException("Datensatz konnte nicht gefunden werden!");

            try 
            {
                _context.Remove(existingEntity);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new UpdateException("Datensatz konnte nicht gefunden werden!", ex);
            }
            catch (DbUpdateException ex)
            {
                throw new UpdateException("Änderungen konnten nicht gespeichert werden!", ex);
            }
        }
    }
}
