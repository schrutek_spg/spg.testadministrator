﻿using Microsoft.EntityFrameworkCore;
using Spg.TestAdministrator.Business.Domain.Interfaces;
using Spg.TestAdministrator.Business.Domain.Models;
using Spg.TestAdministrator.Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.Business.Application
{
    /// <summary>
    /// Eine abstrakte Baisklasse, die für alle Serviceklassen
    /// verwendet werden kann.
    /// </summary>
    /// <typeparam name="TEntity">Die Instanz einer DB-Entität</typeparam>
    public abstract class ServiceBase<TEntity> : IReadOnlyService<TEntity>
        where TEntity : class, new()
    {
        /// <summary>
        /// Hält die Instanz des Datenbank-Kontetes
        /// </summary>
        private readonly TestsAdministratorContext _dbContext;

        /// <summary>
        /// Constructor-Injection
        /// </summary>
        /// <param name="dbContext"></param>
        public ServiceBase(TestsAdministratorContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Stellt (generisch) die Datenbanktabelle zur Verfügung.
        /// Das Ergebnis bleibt aber durch den Type des Interfaces 
        /// <code>IQueryable</code> für den Aufrufer weiter abfragefähig.
        /// </summary>
        /// <returns>Die ersten 100 Datensätze</returns>
        public IQueryable<TEntity> Table(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            IQueryable<TEntity> result = _dbContext
                    .Set<TEntity>();

            if (filter != null)
            {
                result = result.Where(filter);
            }
            if (orderBy != null)
            {
                result = orderBy(result);
            }

            return result;
        }
    }
}
