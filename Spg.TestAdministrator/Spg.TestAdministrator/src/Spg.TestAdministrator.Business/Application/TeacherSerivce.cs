﻿using Microsoft.EntityFrameworkCore;
using Spg.TestAdministrator.Business.Domain.Exceptions;
using Spg.TestAdministrator.Business.Domain.Interfaces;
using Spg.TestAdministrator.Business.Domain.Models;
using Spg.TestAdministrator.Business.Infrastructure;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.Business.Application
{
    public class TeacherSerivce : ServiceBase<Teacher>, IReadOnlyTeacherService<Teacher>
    {
        private readonly TestsAdministratorContext _context;

        public TeacherSerivce(TestsAdministratorContext context)
            : base(context)
        {
            _context = context;
        }

        public async Task<Test> CreateAsync(Test newEntity)
        {
            throw new NotImplementedException();
        }

        public async Task<Teacher> UpdateAsync(string id, Teacher newEntity)
        {
            if (id != newEntity.T_ID)
            {
                throw new UpdateException("Datensatz konnte nicht gefunden werden!");
            }

            Teacher existingEntity = await _context.Teachers.FindAsync(id);
            if (existingEntity != null)
            {
                try
                {
                    //existingEntity.C_Department = newEntity.TE_Subject;
                    //existingEntity.C_ClassTeacher = newSchoolclass.C_ClassTeacher;

                    _context.Update(existingEntity);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    throw new UpdateException("Datensatz konnte nicht gefunden werden!", ex);
                }
                catch (DbUpdateException ex)
                {
                    throw new UpdateException("Änderungen konnten nicht gespeichert werden!", ex);
                }
            }
            throw new UpdateException("Datensatz konnte nicht gefunden werden!");
        }

        public async Task DeleteAsync(long id)
        {
            throw new NotImplementedException();
        }
    }
}
