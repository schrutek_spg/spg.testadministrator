﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Spg.TestAdministrator.Business.Domain.Exceptions
{
    public class UpdateException : Exception
    {
        public UpdateException()
            : base()
        { }

        public UpdateException(string? message)
            : base(message)
        { }

        public UpdateException(string? message, Exception? innerException)
            : base(message, innerException)
        { }
    }
}
