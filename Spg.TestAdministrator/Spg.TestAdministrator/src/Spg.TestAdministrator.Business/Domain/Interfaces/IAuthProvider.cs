﻿using Spg.TestAdministrator.Business.Domain.Models.Custom;
using System;
using System.Collections.Generic;
using System.Text;

namespace Spg.TestAdministrator.Business.Domain.Interfaces
{
    public interface IAuthProvider
    {
        (UserInfo userInfo, string errorMessage) CheckUser(string eMail, string password);
    }
}
