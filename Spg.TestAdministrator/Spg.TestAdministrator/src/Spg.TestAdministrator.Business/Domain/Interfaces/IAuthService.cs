﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.Business.Domain.Interfaces
{
    public interface IAuthService
    {
        public bool IsAuthenticated { get; }

        public string Username();

        public string TeacherId();

        Task LoginAsync(string username, string password);

        Task LogOutAsync();

        bool HasRole(string role);

        //...
    }
}
