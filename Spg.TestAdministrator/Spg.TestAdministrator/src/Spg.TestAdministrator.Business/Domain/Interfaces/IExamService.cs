﻿using Spg.TestAdministrator.Business.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.Business.Domain.Interfaces
{
    /// <summary>
    /// Interface für die Write-Actions.
    /// </summary>
    /// <remarks>
    /// Write-Actions stehen selten ohne Read-Actions alleine zur Verfügung
    /// (siehe Update-Methode). Deshalb stellt man zu den "bösen" Write-Actions
    /// auch oft Read-Actions mit zur Verfügung. Hier durch Vererbung gelöst.
    /// </remarks>
    /// <typeparam name="TEntity">Die Instanz einer DB-Entität</typeparam>
    public interface IExamService<TEntity> : IReadOnlyExamService<TEntity>
        where TEntity : class, new()
    {
        /// <summary>
        /// Fügt einen neuen Datensatz hinzu
        /// </summary>
        /// <param name="newEntity"></param>
        /// <returns></returns>
        Task<TEntity> CreateAsync(TEntity newEntity);

        /// <summary>
        /// Aktualisiert einen vorhandenen Datensatz
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newEntity"></param>
        /// <returns></returns>
        Task<TEntity> UpdateAsync(Guid id, TEntity newEntity);

        /// <summary>
        /// Löscht einen vorhandenen Datensatz
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteAsync(Guid id);
    }
}