﻿using Spg.TestAdministrator.Business.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Spg.TestAdministrator.Business.Domain.Interfaces
{
    /// <summary>
    /// Interface, das ausschließlich Read-Actions zur Verfügung stellt
    /// </summary>
    /// <remarks>
    /// Das Interface ist generisch gestaltet, da die Methode <code>Table()</code> 
    /// einen generischen Rückgabetyp verlangt.
    /// </remarks>
    /// <typeparam name="TEntity">Die Instanz einer DB-Entität</typeparam>
    public interface IReadOnlyExamService<TEntity> : IReadOnlyService<TEntity>
        where TEntity : class, new()
    {
        Test GetById(Guid id);
    }
}
