﻿using Spg.TestAdministrator.Business.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spg.TestAdministrator.Business.Domain.Interfaces
{
    public interface IReadOnlyLessonService<TEntity> : IReadOnlyService<TEntity>
        where TEntity : class, new()
    {
        IQueryable<Lesson> ListByTeacher();
    }
}
