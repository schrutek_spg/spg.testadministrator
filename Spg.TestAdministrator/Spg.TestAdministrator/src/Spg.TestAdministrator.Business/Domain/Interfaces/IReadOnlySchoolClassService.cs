﻿using Spg.TestAdministrator.Business.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.Business.Domain.Interfaces
{
    public interface IReadOnlySchoolClassService<TEntity> : IReadOnlyService<TEntity>
        where TEntity : class, new()
    {
        Task<IEnumerable<Schoolclass>> GetAllAsync();

        Task<Schoolclass> DetailsAsync(string id);

        Task<Schoolclass> UpdateAsync(string id, Schoolclass newSchoolclass);

        Task DeleteAsync(string id);
    }
}
