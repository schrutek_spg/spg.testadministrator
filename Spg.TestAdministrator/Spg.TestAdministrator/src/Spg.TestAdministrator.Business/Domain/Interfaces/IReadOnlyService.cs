﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Spg.TestAdministrator.Business.Domain.Interfaces
{
    public interface IReadOnlyService<TEntity>
        where TEntity : class
    {
        IQueryable<TEntity> Table(
                    Expression<Func<TEntity, bool>> filter = null,
                    Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null);
    }
}
