﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Spg.TestAdministrator.Business.Domain.Interfaces
{
    public interface IReadOnlyTeacherService<TEntity> : IReadOnlyService<TEntity>
        where TEntity : class
    { }
}
