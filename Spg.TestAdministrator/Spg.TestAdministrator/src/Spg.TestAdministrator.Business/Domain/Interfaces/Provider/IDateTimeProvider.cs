﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Spg.TestAdministrator.Business.Domain.Interfaces.Provider
{
    public interface IDateTimeProvider
    {
        DateTime UtcNow { get; }
    }
}
