﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Spg.TestAdministrator.Business.Domain.Models.Custom
{
    public enum UserRoles { Pupil, Teacher }

    public record UserInfo(
        string EMail,
        string TeacherId,
        UserRoles UserRole
        );
}
