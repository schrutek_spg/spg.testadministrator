﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Spg.TestAdministrator.Business.Domain.Models
{
    public partial class Lesson
    {
        public string LessonDescription { get { return $"{L_ID} F:{L_Subject} ({L_Day}/{L_Hour} R:{L_Room} T:{L_Teacher})"; } }
    }
}
