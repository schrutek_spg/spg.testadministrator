﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Spg.TestAdministrator.Business.Domain.Models
{
    public partial class Teacher
    {
        public string FullName { get { return $"{T_Firstname} {T_Lastname}"; } }
    }
}