﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Spg.TestAdministrator.Business.Domain.Models
{
    [Index(nameof(TE_Teacher), Name = "IX_Users_TE_Teacher")]
    public partial class User
    {
        public User()
        {
            InverseLaseChangeUser = new HashSet<User>();
            Pupils = new HashSet<Pupil>();
            Teachers = new HashSet<Teacher>();
            Tests = new HashSet<Test>();
        }

        [Key]
        public Guid UserId { get; set; }
        public DateTime? LastChangeDate { get; set; }
        public Guid? LaseChangeUserId { get; set; }
        public DateTime RegisterDateTime { get; set; }
        [Required]
        [StringLength(200)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(200)]
        public string LastName { get; set; }
        public string EMail { get; set; }
        public string PasswordHash { get; set; }
        public string Salt { get; set; }
        [Required]
        [StringLength(200)]
        public string Role { get; set; }
        [StringLength(8)]
        public string TE_Teacher { get; set; }

        [ForeignKey(nameof(LaseChangeUserId))]
        [InverseProperty(nameof(User.InverseLaseChangeUser))]
        public virtual User LaseChangeUser { get; set; }
        [ForeignKey(nameof(TE_Teacher))]
        [InverseProperty(nameof(Teacher.Users))]
        public virtual Teacher TE_TeacherNavigation { get; set; }
        [InverseProperty(nameof(User.LaseChangeUser))]
        public virtual ICollection<User> InverseLaseChangeUser { get; set; }
        [InverseProperty(nameof(Pupil.P_LastChangeUser))]
        public virtual ICollection<Pupil> Pupils { get; set; }
        [InverseProperty(nameof(Teacher.T_LastChangeUser))]
        public virtual ICollection<Teacher> Teachers { get; set; }
        [InverseProperty(nameof(Test.TE_LastChangeUser))]
        public virtual ICollection<Test> Tests { get; set; }
    }
}
