﻿using Spg.TestAdministrator.Business.Application.Helpers;
using Spg.TestAdministrator.Business.Domain.Interfaces;
using Spg.TestAdministrator.Business.Domain.Models;
using Spg.TestAdministrator.Business.Domain.Models.Custom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spg.TestAdministrator.Business.Infrastructure
{
    public class DbAuthProvider : IAuthProvider
    {
        private readonly TestsAdministratorContext _dbContext;

        public DbAuthProvider(TestsAdministratorContext dbContext)
        {
            _dbContext = dbContext;
        }

        public (UserInfo userInfo, string errorMessage) CheckUser(string username, string password)
        {
            string message = string.Empty;

            User existingUser = _dbContext.Users.SingleOrDefault(u => u.EMail == username);
            if (existingUser != null)
            {
                if (HashHelper.GenerateHash(password, existingUser.Salt) != existingUser.PasswordHash)
                {
                    message = "Die Anmeldung ist fehlgeschlagen!";
                }
                else
                {
                    UserRoles userRole = UserRoles.Pupil;
                    switch (existingUser.Role.ToUpper())
                    {
                        case "TEACHER":
                            userRole = UserRoles.Teacher;
                            break;
                    }
                    return (new UserInfo(existingUser.EMail, existingUser.TE_Teacher, userRole), message);
                }
            }
            else
            {
                message = "Die Anmeldung ist fehlgeschlagen!";
            }
            return (null, message);
        }
    }
}
