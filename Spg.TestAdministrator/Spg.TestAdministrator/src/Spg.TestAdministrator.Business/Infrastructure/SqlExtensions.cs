﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Spg.TestAdministrator.Business.Infrastructure
{
    public static class SqlExtensions
    {
        public static void ConfigureSql(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<TestsAdministratorContext>(optionsBuilder =>
            {
                if (!optionsBuilder.IsConfigured)
                {
                    optionsBuilder.UseSqlServer(connectionString);
                }
            });
        }
    }
}
