﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Spg.TestAdministrator.Business.Migrations
{
    public partial class Test_GUID_added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Test",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: Guid.NewGuid());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Test");
        }
    }
}
