﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Spg.TestAdministrator.Business.Domain.Exceptions;
using Spg.TestAdministrator.Business.Domain.Interfaces;
using Spg.TestAdministrator.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.WebApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAuthService _authService;
        private readonly ILogger _logger;
        private readonly HttpContext _httpContext;

        public AccountController(IAuthService authService, ILogger<AccountController> logger)
        {
            _authService = authService;
            _logger = logger;
        }

        [HttpGet()]
        public async Task<IActionResult> Login()
        {
            // Clear the existing external cookie
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return View();
        }

        [HttpPost()]
        [ValidateAntiForgeryToken()]
        public async Task<IActionResult> Login([FromForm] ApplicationUserDto model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _authService.LoginAsync(model.Email, model.Password);
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        // Senden von 302 Redirect (nicht RedirectToPage, denn hier würde kein neuer Request
                        // des Browsers gestartet werden).
                        return RedirectToLocal(returnUrl);
                    }
                    return RedirectToAction(nameof(Index), "Home");
                }
                catch (AuthServiceException e)
                {
                    ModelState.AddModelError("", e.Message);
                }
                return View();
            }
            // Something failed. Redisplay the form.
            return View();
        }

        [HttpPost()]
        [ValidateAntiForgeryToken()]
        public async Task<IActionResult> Logout()
        {
            _logger.LogInformation("User {Name} logged out at {Time}.", User.Identity.Name, DateTime.UtcNow);

            await _authService.LogOutAsync();

            return LocalRedirect("/Account/SignedOut");
        }

        [HttpGet()]
        public IActionResult SignedOut()
        {
            if (User.Identity.IsAuthenticated)
            {
                // Redirect to home page if the user is authenticated.
                return RedirectToPage("/Index");
            }
            return View();
        }

        public IActionResult AccessDenied(string returnUrl)
        {
            return View("AccessDenied", returnUrl);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
