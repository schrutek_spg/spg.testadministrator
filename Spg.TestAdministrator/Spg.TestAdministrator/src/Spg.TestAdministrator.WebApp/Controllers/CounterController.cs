﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.WebApp.Controllers
{
    public class CounterController : Controller
    {
        public IActionResult Index()
        {
            string currentDate = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss");

            return View(currentDate as object);
        }
    }
}
