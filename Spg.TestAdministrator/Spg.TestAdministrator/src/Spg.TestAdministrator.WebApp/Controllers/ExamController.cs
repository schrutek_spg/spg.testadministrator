﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Spg.TestAdministrator.Business.Application;
using Spg.TestAdministrator.Business.Domain.Exceptions;
using Spg.TestAdministrator.Business.Domain.Interfaces;
using Spg.TestAdministrator.Business.Domain.Models;
using Spg.TestAdministrator.WebApp.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.WebApp.Controllers
{
    public class ExamController : Controller
    {
        private readonly IExamService<Test> _examService;
        private readonly IReadOnlyExamService<Test> _readOnlyExamService;
        private readonly IReadOnlyTeacherService<Teacher> _readOnlyTeacherService;
        private readonly IReadOnlySchoolClassService<Schoolclass> _readOnlySchoolClassService;
        private readonly IReadOnlyLessonService<Lesson> _readOnlyLessonService;

        public ExamController(
            IReadOnlyExamService<Test> readOnlyExamService, 
            IExamService<Test> examService,
            IReadOnlyTeacherService<Teacher> readOnlyTeacherService,
            IReadOnlySchoolClassService<Schoolclass> readOnlySchoolClassService,
            IReadOnlyLessonService<Lesson> readOnlyLessonService)
        {
            _examService = examService;
            _readOnlyExamService = readOnlyExamService;
            _readOnlyTeacherService = readOnlyTeacherService;
            _readOnlySchoolClassService = readOnlySchoolClassService;
            _readOnlyLessonService = readOnlyLessonService;
        }

        public async Task<IActionResult> AjaxIndex()
        {
            IQueryable<Test> result = _examService
                .Table(null, null)
                .Include(t => t.TE_TeacherNavigation);

            PagenatedList<Test> model = await PagenatedList<Test>.CreateAsync(result, 1, 10);

            return View(model);
        }

        public async Task<IActionResult> IndexPartial(string filter, string sortOrder, int pageIndex = 1)
        {
            ViewData["CurrentSort"] = sortOrder;

            ViewData["IdSortParm"] = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewData["DateSortParm"] = sortOrder == "date" ? "date_desc" : "date";
            ViewData["SubjectSortParm"] = sortOrder == "subject" ? "subject_desc" : "subject";

            Expression<Func<Test, bool>> filterExpression = null;
            if (!string.IsNullOrEmpty(filter))
            {
                filterExpression = t => t.TE_Teacher.Contains(filter)
                    || t.TE_TeacherNavigation.T_Lastname.Contains(filter);
            }

            Func<IQueryable<Test>, IOrderedQueryable<Test>> sortOrderExpression = null;
            if (!string.IsNullOrEmpty(sortOrder))
            {
                switch (sortOrder)
                {
                    case "date":
                        sortOrderExpression = l => l.OrderBy(s => s.TE_Date);
                        break;
                    case "date_desc":
                        sortOrderExpression = l => l.OrderByDescending(s => s.TE_Date);
                        break;
                    case "subject":
                        sortOrderExpression = l => l.OrderBy(s => s.TE_Subject);
                        break;
                    case "subject_desc":
                        sortOrderExpression = l => l.OrderByDescending(s => s.TE_Subject);
                        break;
                    case "id_desc":
                        sortOrderExpression = l => l.OrderByDescending(s => s.TE_ID);
                        break;
                    default:
                        sortOrderExpression = l => l.OrderBy(s => s.TE_ID);
                        break;
                }
            }

            IQueryable<Test> result = _examService
                .Table(filterExpression, sortOrderExpression)
                .Include(t => t.TE_TeacherNavigation);

            PagenatedList<Test> model = await PagenatedList<Test>.CreateAsync(result, pageIndex, 10);

            return PartialView("_ExamList", model);
        }

        public IActionResult VueIndex()
        {
            return View();
        }

        public async Task<IActionResult> VueTableContent(string filter, string sortField, int pageIndex = 1)
        {
            Expression<Func<Test, bool>> filterExpression = null;
            if (!string.IsNullOrEmpty(filter))
            {
                filterExpression = t => t.TE_Teacher.Contains(filter)
                    || t.TE_TeacherNavigation.T_Lastname.Contains(filter);
            }

            Func<IQueryable<Test>, IOrderedQueryable<Test>> sortOrderExpression = null;
            if (!string.IsNullOrEmpty(sortField))
            {
                sortOrderExpression = sortField switch
                {
                    "date" => l => l.OrderBy(s => s.TE_Date),
                    "date_desc" => l => l.OrderByDescending(s => s.TE_Date),
                    "subject" => l => l.OrderBy(s => s.TE_Subject),
                    "subject_desc" => l => l.OrderByDescending(s => s.TE_Subject),
                    "id_desc" => l => l.OrderByDescending(s => s.TE_ID),
                    _ => l => l.OrderBy(s => s.TE_ID),
                };
            }

            IQueryable<Test> result = _examService
                .Table(filterExpression, sortOrderExpression)
                .Include(t => t.TE_TeacherNavigation);

            PagenatedList<Test> model = await PagenatedList<Test>.CreateAsync(result, pageIndex, 10);

            return Ok(new ExamListDto()
            {
                Exams = model.Select(t => new TestDto()
                {
                    TE_ID = t.TE_ID,
                    TE_Date = t.TE_Date,
                    TE_Subject = t.TE_Subject,
                    TE_TeacherNavigation = TeacherDto.Map(t.TE_TeacherNavigation)
                })
                .ToList(),
                TotalPages = model.TotalPages
            });
        }


        [HttpGet()]
        public async Task<IActionResult> Index(string filter, string currentFilter, string sortOrder, int pageIndex = 1)
        {
            ViewData["CurrentFilter"] = currentFilter;
            ViewData["CurrentSort"] = sortOrder;

            ViewData["IdSortParm"] = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewData["DateSortParm"] = sortOrder == "date" ? "date_desc" : "date";
            ViewData["SubjectSortParm"] = sortOrder == "subject" ? "subject_desc" : "subject";

            filter = filter ?? currentFilter;

            Expression<Func<Test, bool>> filterExpression = null;
            if (!string.IsNullOrEmpty(filter))
            {
                filterExpression = t => t.TE_Teacher.Contains(filter)
                    || t.TE_TeacherNavigation.T_Lastname.Contains(filter);

                ViewData["CurrentFilter"] = filter;
            }

            Func<IQueryable<Test>, IOrderedQueryable<Test>> sortOrderExpression = null;
            if (!string.IsNullOrEmpty(sortOrder))
            {
                switch (sortOrder)
                {
                    case "date":
                        sortOrderExpression = l => l.OrderBy(s => s.TE_Date);
                        break;
                    case "date_desc":
                        sortOrderExpression = l => l.OrderByDescending(s => s.TE_Date);
                        break;
                    case "subject":
                        sortOrderExpression = l => l.OrderBy(s => s.TE_Subject);
                        break;
                    case "subject_desc":
                        sortOrderExpression = l => l.OrderByDescending(s => s.TE_Subject);
                        break;
                    case "id_desc":
                        sortOrderExpression = l => l.OrderByDescending(s => s.TE_ID);
                        break;
                    default:
                        sortOrderExpression = l => l.OrderBy(s => s.TE_ID);
                        break;
                }
            }

            IQueryable<Test> result = _readOnlyExamService
                .Table(filterExpression, sortOrderExpression)
                .Include(t => t.TE_TeacherNavigation);

            PagenatedList<Test> model = await PagenatedList<Test>.CreateAsync(result, pageIndex, 10);

            return View(model);
        }

        [HttpGet()]
        [Authorize(Policy = "PolicyForTeachers")]
        public async Task<IActionResult> Edit(Guid id)
        {
            // TODO: Woher die Daten nehmen, View aufrufen
            Test model = _examService.GetById(id);
            if (model == null)
            {
                return NotFound();
            }

            //TODO: Für die Class und für die Lesson die Drop-Down-Boxes befüllen
            ViewData["TE_Teacher"] = new SelectList(_readOnlyTeacherService.Table(null, null), nameof(Teacher.T_ID), nameof(Teacher.FullName));
            ViewData["TE_Class"] = new SelectList(_readOnlySchoolClassService.Table(null, null), "C_ID", "C_ID");
            ViewData["TE_Lesson"] = new SelectList(await _readOnlyLessonService.ListByTeacher().ToListAsync(), "L_ID", "LessonDescription");

            return View(model);
        }

        [HttpPost()]
        [Authorize(Policy = "PolicyForTeachers")]
        public async Task<IActionResult> Edit(Guid id, Test model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                await _examService.UpdateAsync(id, model);
            }
            catch (UpdateException ex)
            {
                ModelState.AddModelError("", ex.Message);
                TempData["Error"] = $"{ex.Message} (aus TempData)";

                ViewData["TE_Teacher"] = new SelectList(_readOnlyTeacherService.Table(null, null), nameof(Teacher.T_ID), nameof(Teacher.FullName));
                ViewData["TE_Class"] = new SelectList(_readOnlySchoolClassService.Table(null, null), "C_ID", "C_ID");
                ViewData["TE_Lesson"] = new SelectList(_readOnlyLessonService.Table(null, null), "L_ID", "L_ID");

                return View(model);
            }
            return RedirectToAction(nameof(Index));
        }
    }
}