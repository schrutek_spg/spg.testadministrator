﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Spg.TestAdministrator.WebApp.Dtos;
using Spg.TestAdministrator.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.WebApp.Controllers
{
    public class LoginController : Controller
    {
        [HttpGet()]
        public IActionResult Index()
        {
            string username = Request.Cookies["UserName"];
            return View(new LoginDto() { UserName = username });
        }

        [HttpPost()]
        public IActionResult Index(LoginDto model)
        {
            string username = model.UserName;

            if (username == "martin")
            {
                CookieOptions cookieOptions = new CookieOptions();
                cookieOptions.Expires = DateTime.Now.AddMinutes(15);
                Response.Cookies.Append("UserName", username, cookieOptions);

                LoginDto dto = new LoginDto() { UserName = username };

                return View(dto);
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpGet()]
        public IActionResult LogOut()
        {
            Response.Cookies.Delete("UserName");
            return View(nameof(Index), new LoginDto());
        }
    }
}
