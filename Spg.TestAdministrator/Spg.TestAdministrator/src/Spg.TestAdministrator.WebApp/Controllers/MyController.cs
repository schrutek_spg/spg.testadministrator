﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.WebApp.Controllers
{
    public class MyController : Controller
    {
        public IActionResult Index()
        {
            return View("MyIndex", new DateTime());
        }
    }
}
