﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Spg.TestAdministrator.Business.Application;
using Spg.TestAdministrator.Business.Domain.Models;
using Spg.TestAdministrator.Business.Infrastructure;

namespace Spg.TestAdministrator.WebApp.Controllers
{
    public class SchoolclassesController : Controller
    {
        private readonly TestsAdministratorContext _context;
        private readonly SchoolclassService _schoolclassService;

        public SchoolclassesController(TestsAdministratorContext context, SchoolclassService schoolclassService)
        {
            _context = context;
            _schoolclassService = schoolclassService;
        }

        // GET: Schoolclasses
        public async Task<IActionResult> Index()
        {
            List<Schoolclass> model = await _context.Schoolclasses
                .Include(s => s.C_ClassTeacherNavigation)
                .ToListAsync();

            return View(model);
        }

        // GET: Schoolclasses/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolclass = await _context.Schoolclasses
                .Include(s => s.C_ClassTeacherNavigation)
                .FirstOrDefaultAsync(m => m.C_ID == id);
            if (schoolclass == null)
            {
                return NotFound();
            }

            return View(schoolclass);
        }

        // GET: Schoolclasses/Create
        public IActionResult Create()
        {
            ViewData["C_ClassTeacher"] = new SelectList(_context.Teachers, "T_ID", "T_ID");
            return View();
        }

        // POST: Schoolclasses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("C_ID,C_Department,C_ClassTeacher")] Schoolclass schoolclass)
        {
            if (ModelState.IsValid)
            {
                _context.Add(schoolclass);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["C_ClassTeacher"] = new SelectList(_context.Teachers, "T_ID", "T_ID", schoolclass.C_ClassTeacher);
            return View(schoolclass);
        }

        // GET: Schoolclasses/Edit/5
        [HttpGet()]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolclass = await _context.Schoolclasses.FindAsync(id);
            if (schoolclass == null)
            {
                return NotFound();
            }
            ViewData["C_ClassTeacher"] = new SelectList(_context.Teachers, "T_ID", "T_ID", schoolclass.C_ClassTeacher);
            return View(schoolclass);
        }

        // POST: Schoolclasses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost()]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("C_ID,C_Department,C_ClassTeacher")] Schoolclass schoolclass)
        {
            if (id != schoolclass.C_ID)
            {
                return NotFound();
            }

            // TODO: ModelState erklären
            if (ModelState.IsValid)
            {
                await _schoolclassService.UpdateAsync(id, schoolclass);
                return RedirectToAction(nameof(Index));
            }
            ViewData["C_ClassTeacher"] = new SelectList(_context.Teachers, "T_ID", "T_ID", schoolclass.C_ClassTeacher);
            return View(schoolclass);
        }

        // GET: Schoolclasses/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolclass = await _context.Schoolclasses
                .Include(s => s.C_ClassTeacherNavigation)
                .FirstOrDefaultAsync(m => m.C_ID == id);
            if (schoolclass == null)
            {
                return NotFound();
            }

            return View(schoolclass);
        }

        // POST: Schoolclasses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var schoolclass = await _context.Schoolclasses.FindAsync(id);
            _context.Schoolclasses.Remove(schoolclass);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SchoolclassExists(string id)
        {
            return _context.Schoolclasses.Any(e => e.C_ID == id);
        }
    }
}
