﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Spg.TestAdministrator.Business.Application;
using Spg.TestAdministrator.Business.Domain.Interfaces;
using Spg.TestAdministrator.Business.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.WebApp.Controllers
{
    public class TeacherController : Controller
    {
        private readonly IReadOnlyTeacherService<Teacher> _readOnlyTeacherService;

        public TeacherController(IReadOnlyTeacherService<Teacher> readOnlyTeacherService)
        {
            _readOnlyTeacherService = readOnlyTeacherService;
        }

        public async Task<IActionResult> Index(string filter, string currentFilter, string sortOrder, int pageIndex = 1)
        {
            TempData["Message"] = "Message form Index-Method!";

            ViewBag.CurrentFilter = "asdasdasdasdasdasasdasdasdasd";

            ViewData["CurrentFilter"] = currentFilter;
            ViewData["CurrentSort"] = sortOrder;

            ViewData["FirstNameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "firstname_desc" : "";
            ViewData["LastNameSortParm"] = sortOrder == "lastname" ? "lastname_desc" : "lastname";
            ViewData["EMailSortParm"] = sortOrder == "email" ? "email_desc" : "email";

            Expression<Func<Teacher, bool>> filterExpression = null;
            if (!string.IsNullOrEmpty(filter))
            {
                filterExpression = t => t.T_Firstname.Contains(filter) 
                    || t.T_Lastname.Contains(filter) 
                    || t.T_Email.Contains(filter)
                    || t.T_ID.Contains(filter);

                ViewData["CurrentFilter"] = filter;
            }

            Func<IQueryable<Teacher>, IOrderedQueryable<Teacher>> sortOrderExpression = null;
            if (!string.IsNullOrEmpty(sortOrder))
            {
                switch (sortOrder)
                {
                    case "lastname":
                        sortOrderExpression = l => l.OrderBy(s => s.T_Lastname);
                        break;
                    case "lastname_desc":
                        sortOrderExpression = l => l.OrderByDescending(s => s.T_Lastname);
                        break;
                    case "email":
                        sortOrderExpression = l => l.OrderBy(s => s.T_Email);
                        break;
                    case "email_desc":
                        sortOrderExpression = l => l.OrderByDescending(s => s.T_Email);
                        break;
                    case "firstname_desc":
                        sortOrderExpression = l => l.OrderByDescending(s => s.T_Firstname);
                        break;
                    default:
                        sortOrderExpression = l => l.OrderBy(s => s.T_Firstname);
                        break;
                }
            }

            try
            {
                IQueryable<Teacher> result = _readOnlyTeacherService
                    .Table(filterExpression, sortOrderExpression)
                    .Include(t => t.Lessons)
                    .Include(t => t.T_CatAccountState);

                PagenatedList<Teacher> model = await PagenatedList<Teacher>.CreateAsync(result, pageIndex, 10);

                return View(model);
            }
            catch (ArgumentNullException ex)
            {
                TempData["Message"] = "Something went wrong: " + ex.StackTrace; //Loggen (Log-File, ...)
            }
            return View();
        }

        public IActionResult Details(string id)
        {
            string message = TempData["Message"]?.ToString();

            return View("Details", message);
        }
    }
}
