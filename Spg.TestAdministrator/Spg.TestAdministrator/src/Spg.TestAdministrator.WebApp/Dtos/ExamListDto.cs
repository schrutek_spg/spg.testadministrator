﻿using Spg.TestAdministrator.Business.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.WebApp.Dtos
{
    public class ExamListDto
    {
        public List<TestDto> Exams { get; set; } = null!;

        public int TotalPages { get; set; }

        public string SortOrder { get; set; } = null!;
    }
}
