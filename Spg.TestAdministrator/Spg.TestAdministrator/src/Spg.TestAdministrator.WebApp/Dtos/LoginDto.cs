﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.WebApp.Dtos
{
    public class LoginDto
    {
        [Required]
        [StringLength(150)]
        public string UserName { get; set; }
    }
}
