﻿using Spg.TestAdministrator.Business.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.WebApp.Dtos
{
    public class TeacherDto
    {
        public string T_Firstname { get; set; }

        public string T_Lastname { get; set; }

        public static TeacherDto Map(Teacher entity)
        {
            TeacherDto dto = new TeacherDto();
            dto.T_Firstname = entity.T_Firstname;
            dto.T_Lastname = entity.T_Lastname;
            return dto;
        }
    }
}
