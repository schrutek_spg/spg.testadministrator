﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.WebApp.Dtos
{
    public class TestDto
    {
        [Required]
        public long TE_ID { get; set; }

        public DateTime TE_Date { get; set; }

        [Required]
        [StringLength(8)]
        public string TE_Teacher { get; set; }

        public TeacherDto TE_TeacherNavigation { get; set; }

        [Required]
        [StringLength(8)]
        public string TE_Subject { get; set; }
    }
}
