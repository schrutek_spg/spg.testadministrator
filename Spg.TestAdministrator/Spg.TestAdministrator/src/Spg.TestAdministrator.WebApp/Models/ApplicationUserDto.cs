﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.WebApp.Models
{
    public class ApplicationUserDto
    {
        [Required]
        [StringLength(150)]
        public string Email { get; set; }

        [Required]
        [StringLength(150)]
        public string Password { get; set; }

        [StringLength(150)]
        public string FullName { get; set; }
    }
}
