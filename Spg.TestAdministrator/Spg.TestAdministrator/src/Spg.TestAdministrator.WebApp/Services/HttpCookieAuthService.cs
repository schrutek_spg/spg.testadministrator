﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Spg.TestAdministrator.Business.Domain.Exceptions;
using Spg.TestAdministrator.Business.Domain.Interfaces;
using Spg.TestAdministrator.Business.Domain.Models;
using Spg.TestAdministrator.Business.Domain.Models.Custom;
using Spg.TestAdministrator.Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.WebApp.Services
{
    public class HttpCookieAuthService : IAuthService
    {
        private readonly TestsAdministratorContext _dbContext;
        private readonly HttpContext _httpContext;
        private readonly IAuthProvider _authProvider;

        public bool IsAuthenticated 
            => _httpContext.User.Identity.IsAuthenticated;

        public HttpCookieAuthService(
            TestsAdministratorContext dbContext,
            IAuthProvider authProvider,
            IHttpContextAccessor httpContext)
        {
            _dbContext = dbContext;
            _authProvider = authProvider;
            _httpContext = httpContext.HttpContext;
        }

        public async Task LoginAsync(string eMail, string password)
        {
            var (userInfo, errorMessage) = _authProvider.CheckUser(eMail, password);
            if (string.IsNullOrEmpty(errorMessage))
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, userInfo.EMail),
                    new Claim(nameof(User), JsonSerializer.Serialize(userInfo)),
                    new Claim(ClaimTypes.Role, userInfo.UserRole.ToString()),
                };

                var claimsIdentity = new ClaimsIdentity(
                    claims,
                    Microsoft.AspNetCore.Authentication.Cookies.CookieAuthenticationDefaults.AuthenticationScheme);

                // 3h dauert die Session. Kann natürlich auch anders konfiguriert werden.
                var authProperties = new AuthenticationProperties
                {
                    ExpiresUtc = DateTimeOffset.UtcNow.AddHours(3),
                };

                await _httpContext.SignInAsync(
                    Microsoft.AspNetCore.Authentication.Cookies.CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(claimsIdentity),
                    authProperties);
            }
            else
            {
                throw new AuthServiceException(errorMessage);
            }
        }

        public Task LogOutAsync()
        {
            return _httpContext.SignOutAsync(
                Microsoft.AspNetCore.Authentication.Cookies.CookieAuthenticationDefaults.AuthenticationScheme);
        }

        /// <summary>
        /// Gibt den aktuellen Benutzernamen zurück. Null wenn kein Benutzer angemeldet ist.
        /// </summary>
        public string Username()
            => _httpContext.User.Identity.Name;

        public string TeacherId()
        {
            string userInfoString = _httpContext.User.Claims.SingleOrDefault(c => c.Type == nameof(User))?.Value;
            if (!string.IsNullOrEmpty(userInfoString))
            {
                UserInfo userInfo = JsonSerializer.Deserialize<UserInfo>(userInfoString);
                return userInfo?.TeacherId;
            }
            return null;
        }

        /// <summary>
        /// Prüft, ob der aktuelle Benutzer die übergebene Rolle besitzt.
        /// </summary>
        public bool HasRole(string role)
            => _httpContext.User.IsInRole(role.ToString());
   }
}