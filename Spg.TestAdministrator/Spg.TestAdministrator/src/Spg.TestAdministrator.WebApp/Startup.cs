using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Spg.TestAdministrator.Business.Application;
using Spg.TestAdministrator.Business.Application.Provider;
using Spg.TestAdministrator.Business.Domain.Interfaces;
using Spg.TestAdministrator.Business.Domain.Interfaces.Provider;
using Spg.TestAdministrator.Business.Domain.Models;
using Spg.TestAdministrator.Business.Domain.Models.Custom;
using Spg.TestAdministrator.Business.Infrastructure;
using Spg.TestAdministrator.WebApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spg.TestAdministrator.WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            // alte unsaubere variante, weil direkte Verbindung von Presentation zu Infrastructure (X)
            //services.AddDbContext<TestsAdministratorContext>(optionsBuilder => optionsBuilder.UseSqlServer(Configuration["AppSettings:Database"]));
            // besser!!
            services.ConfigureSql(Configuration["AppSettings:Database"]);


            /// *** Authentication, Authorization hinzufügen
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie();

            services.AddAuthorization(o =>
            {
                o.AddPolicy("PolicyForStudents", p => p.RequireRole(UserRoles.Pupil.ToString(), UserRoles.Teacher.ToString()));
                o.AddPolicy("PolicyForTeachers", p => p.RequireRole(UserRoles.Teacher.ToString()));
            });
            /// ***


            services.AddTransient<IReadOnlySchoolClassService<Schoolclass>, SchoolclassService>();

            services.AddTransient<IReadOnlyExamService<Test>, ExamService>();
            services.AddTransient<IExamService<Test>, ExamService>();

            services.AddTransient<IReadOnlyTeacherService<Teacher>, TeacherSerivce>();
            services.AddTransient<IReadOnlyLessonService<Lesson>, LessonService>();
            //services.AddTransient<IAuthService, AuthServiceTemp>(provider => new AuthServiceTemp("SRM"));
            services.AddTransient<IAuthService, HttpCookieAuthService>();
            services.AddTransient<IAuthProvider, DbAuthProvider>();

            services.AddTransient<IDateTimeProvider, DateTimeProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.Use(async (content, next) => 
            {
                //content.Headers.Add(...)
                await next(); 
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            /// *** Authentication, Authorization aktivieren
            app.UseAuthentication();
            app.UseAuthorization();
            var cookiePolicyOptions = new CookiePolicyOptions
            {
                MinimumSameSitePolicy = SameSiteMode.Lax,
            };
            app.UseCookiePolicy(cookiePolicyOptions);
            /// ***

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
